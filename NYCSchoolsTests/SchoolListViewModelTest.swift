//
//  SchoolLostViewModelText.swift
//  NYCSchoolsTests
//
//  Created by Pooja on 4/17/23.
//

import XCTest
@testable import NYCSchools

final class SchoolListViewModelTest: XCTestCase {

    var viewModel: SchoolsListViewModel!
    
    override func setUpWithError() throws {
        viewModel = SchoolsListViewModel(networkManager: NetworkManager())
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        viewModel = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testProcessSchoolsListResponseError() throws {
        let error = NSError(domain: "NYCSchools", code: 0)
        viewModel.processSchoolsListResponse(.failure(error))
        XCTAssert(viewModel.schoolsList.isEmpty)
    }
    
    func testProcessSchoolsListResponseSuccess() throws {
        let school = NYCSchool(dbn: "DBN", name: "name", address: nil, city: nil, state: nil, zip: nil, longitude: nil, latitude: nil, description: nil, email: nil, website: nil)
        
        viewModel.processSchoolsListResponse(.success([school]))
        XCTAssert(viewModel.schoolsList.count == 1)
    }
}
