//
//  UtilityTest.swift
//  NYCSchoolsTests
//
//  Created by Pooja on 4/17/23.
//

import XCTest
@testable import NYCSchools

final class UtilityTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testStringValueNil() throws {
        let text: String? = nil
        XCTAssert(text.stringValue == "")
    }
    
    func testStringValueEmpty() throws {
        let text: String? = ""
        XCTAssert(text.stringValue == "")
    }
    
    func testStringValueNonNil() throws {
        let text: String? = "ABC"
        XCTAssert(text.stringValue == "ABC")
    }

    func testStringHeight() throws {
        let text = "ABC"
        let height = text.heightWithConstrainedWidth(100, font: UIFont.systemFont(ofSize: 16))
        XCTAssert(height >= 16)
    }
}
