//
//  SchoolDetailsViewModelTest.swift
//  NYCSchoolsTests
//
//  Created by Pooja on 4/17/23.
//

import XCTest
@testable import NYCSchools

final class SchoolDetailsViewModelTest: XCTestCase {

    var viewModel: SchoolDetailsViewModel!
    
    override func setUpWithError() throws {
        let school = NYCSchool(dbn: "DBN", name: "name", address: nil, city: nil, state: nil, zip: nil, longitude: nil, latitude: nil, description: nil, email: nil, website: nil)
        viewModel = SchoolDetailsViewModel(school: school, networkManager: NetworkManager())
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        viewModel = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testProcessSchoolsListResponseError() throws {
        let error = NSError(domain: "NYCSchools", code: 0)
        viewModel.processSatDetailsResponse(.failure(error))
        XCTAssertNil(viewModel.school.satScores)
    }
    
    func testProcessSchoolsListResponseSuccess() throws {
        let sat = SchoolSatScores(satTakers: "1", readingAverageScore: "1", mathsAverageScore: "2", writingAverageScore: "3")
        viewModel.processSatDetailsResponse(.success([sat]))
        XCTAssert(viewModel.school.satScores?.satTakers == "1")
    }
}
