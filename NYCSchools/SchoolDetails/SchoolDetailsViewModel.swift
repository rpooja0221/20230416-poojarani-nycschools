//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import Foundation
import Combine

enum SchoolsSatScoresState {
    case loading
    case scoresUpdated
    case error
}

class SchoolDetailsViewModel: SchoolDetailsViewModelType {
    
    @Published
    var _state: SchoolsSatScoresState
    var state: Published<SchoolsSatScoresState>.Publisher { self.$_state }
    
    var school: NYCSchool
    let networkManager: NetworkManager
    
    var isLoading: Bool {
        if self._state == .loading {
            return true
        } else {
            return false
        }
    }
        
    init(school: NYCSchool, networkManager: NetworkManager) {
        if let _ = school.satScores {
            self._state = .scoresUpdated
        } else {
            self._state = .loading
        }
        self.school = school
        self.networkManager = networkManager
        self.getSatDetails()
    }
    
    func getSatDetails() {
        let path = NetworkConstants.schoolDetailsPath + self.school.dbn
        self.networkManager.request(SchoolSatScores.self, path: path) { [weak self] result in
            guard let self = self else { return }
            self.processSatDetailsResponse(result)
        }
    }
    
    func processSatDetailsResponse(_ result: Result<[SchoolSatScores], Error>) {
        switch result {
        case .success(let list):
            self.school.satScores = list.first
            self._state = .scoresUpdated
        case .failure(_):
            self.school.satScores = nil
            self._state = .error
        }
    }
}
