//
//  SchoolDetailsViewController.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit
import Combine

protocol SchoolDetailsViewModelType {
    var isLoading: Bool { get }
    var school: NYCSchool { get }
    var state: Published<SchoolsSatScoresState>.Publisher { get }
}

class SchoolDetailsViewController: BaseViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SchoolDetailsTableViewCell.self, forCellReuseIdentifier: detailsCellIdentifier)
        tableView.register(SchoolDescriptionTableViewCell.self, forCellReuseIdentifier: descriptionCellIdentifier)
        tableView.register(SchoolSATDetailsTableViewCell.self, forCellReuseIdentifier: satCellIdentifier)
        return tableView
    }()
    
    private let viewModel: SchoolDetailsViewModelType
    private var cancellable: Set<AnyCancellable> =  []
    private let padding: CGFloat = 24.0
    private let descriptionCellIdentifier = "SchoolDescriptionTableViewCell"
    private let detailsCellIdentifier = "SchoolDetailsTableViewCell"
    private let satCellIdentifier = "SchoolSATDetailsTableViewCell"
    
    init(viewModel: SchoolDetailsViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = .clear
        self.addSubviews()
        self.setupConstrains()
        self.subscribeViewModel()
    }
}

private extension SchoolDetailsViewController {
    
    func subscribeViewModel() {
        self.viewModel.state
            .receive(on: RunLoop.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.tableView.reloadData()
            }
            .store(in: &cancellable)
    }
    
    func addSubviews() {
        self.view.addSubview(self.tableView)
    }
    
    func setupConstrains() {
        NSLayoutConstraint.activate([
            self.tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

extension SchoolDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: detailsCellIdentifier, for: indexPath) as? SchoolDetailsTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(school: self.viewModel.school)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: satCellIdentifier, for: indexPath) as? SchoolSATDetailsTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(school: self.viewModel.school, isLoading: self.viewModel.isLoading)
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: descriptionCellIdentifier, for: indexPath) as? SchoolDescriptionTableViewCell else {
                return UITableViewCell()
            }
            cell.setup(school: self.viewModel.school)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension SchoolDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
