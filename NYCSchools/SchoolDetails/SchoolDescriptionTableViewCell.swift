//
//  SchoolDescriptionTableViewCell.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit

class SchoolDescriptionTableViewCell: UITableViewCell {
    
    private let padding: CGFloat = 16
    private let labelPadding: CGFloat = 4
    private let boldFont = UIFont.systemFont(ofSize: 20, weight: .semibold)
    private let labelFont = UIFont.systemFont(ofSize: 16, weight: .regular)
    private var heightConstrait: NSLayoutConstraint?
    
    lazy var overviewLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = boldFont
        label.numberOfLines = 0
        label.text = "Overview"
        return label
    }()
    
    lazy var overviewDetailsLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = labelFont
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .none
        self.selectionStyle = .none
        self.setupSubviews()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(school: NYCSchool) {
        self.overviewDetailsLabel.text = school.description
    }
}

private extension SchoolDescriptionTableViewCell {
    func setupSubviews() {
        self.contentView.addSubview(self.overviewLabel)
        self.contentView.addSubview(self.overviewDetailsLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            self.overviewLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.overviewLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.overviewLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: padding),
        ])
        
        NSLayoutConstraint.activate([
            self.overviewDetailsLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.overviewDetailsLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.overviewDetailsLabel.topAnchor.constraint(equalTo: self.overviewLabel.bottomAnchor, constant: labelPadding),
            self.overviewDetailsLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -padding)
        ])
    }
}
