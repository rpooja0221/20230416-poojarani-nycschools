//
//  SchoolDetailsTableViewCell.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit

class SchoolDetailsTableViewCell: UITableViewCell {
    
    private let padding: CGFloat = 16
    private let labelPadding: CGFloat = 4
    private let font = UIFont.systemFont(ofSize: 16)
    private let boldFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
    private let labelFont = UIFont.systemFont(ofSize: 20, weight: .bold)
    private var heightConstrait: NSLayoutConstraint?
    
    lazy var nameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = labelFont
        label.numberOfLines = 0
        return label
    }()
    
    lazy var textView: UITextView = {
        let textView = UITextView(frame: .zero)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = font
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.dataDetectorTypes = [.address, .phoneNumber]
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .none
        self.selectionStyle = .none
        self.setupSubviews()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(school: NYCSchool) {
        var text = school.addressLine1
        text += "\n"
        text += school.addressLine2
        if let email = school.email {
            text += "\n"
            text += "Email: \(email)"
        }
        if let phone = school.phone {
            text += "\n"
            text += "Phone: \(phone)"
        }
        self.nameLabel.text = school.name
        self.textView.attributedText = self.getAttributedText(text)
        self.heightConstrait?.constant = self.getContainerHeight(label: school.name, text: text)
    }
}

private extension SchoolDetailsTableViewCell {
    func setupSubviews() {
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addSubview(self.textView)
    }
    
    func setupConstraints() {
        self.heightConstrait = self.contentView.heightAnchor.constraint(equalToConstant: 100)
        
        NSLayoutConstraint.activate([
            self.nameLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.nameLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.nameLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: padding),
        ])
        
        NSLayoutConstraint.activate([
            self.textView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.textView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.textView.topAnchor.constraint(equalTo: self.nameLabel.bottomAnchor, constant: 0),
            self.textView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -padding)
        ])
        self.heightConstrait?.isActive = true
    }
    
    func getContainerHeight(label: String, text: String) -> CGFloat {
        let width = UIScreen.main.bounds.width - padding * 2
        var height = text.heightWithConstrainedWidth(width, font: font)
        height += label.heightWithConstrainedWidth(width, font: labelFont)
        height += padding * 3
        return height
    }
    
    func getAttributedText(_ text: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: text)
        let nsText = text as NSString
        let textRange = NSRange(location: 0, length: nsText.length)
        attributedText.addAttribute(NSAttributedString.Key.font, value: font, range: textRange)
        if text.contains("Email") {
            let range = nsText.range(of: "Email: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: boldFont, range: range)
        }
        if text.contains("Phone") {
            let range = nsText.range(of: "Phone: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: boldFont, range: range)
        }
        return attributedText
    }
}

extension SchoolDetailsTableViewCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        true
    }
}
