//
//  SchoolSATDetailsTableViewCell.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit

class SchoolSATDetailsTableViewCell: UITableViewCell {
    
    private let padding: CGFloat = 16
    private let labelPadding: CGFloat = 4
    private let boldFont = UIFont.systemFont(ofSize: 20, weight: .semibold)
    private let labelFont = UIFont.systemFont(ofSize: 16, weight: .regular)
    private let labelBoldFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
    private var heightConstrait: NSLayoutConstraint?
    
    lazy var overviewLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = boldFont
        label.numberOfLines = 0
        label.text = "SAT Scores"
        return label
    }()
    
    lazy var overviewDetailsLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = labelFont
        label.numberOfLines = 0
        return label
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .none
        self.selectionStyle = .none
        self.setupSubviews()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(school: NYCSchool, isLoading: Bool) {
        if let sat = school.satScores {
            self.setupSatScores(sat)
        } else if isLoading {
            self.setupLoading()
        } else {
            self.failedLoading()
        }
        self.setNeedsLayout()
    }
}

private extension SchoolSATDetailsTableViewCell {
    func setupSatScores(_ sat: SchoolSatScores) {
        var text = "Takers: \(sat.satTakers ?? "")"
        text += "\nReading Average: \(sat.readingAverageScore ?? "")"
        text +=  "\nWriting Average: \(sat.writingAverageScore ?? "")"
        text +=  "\nMath Average: \(sat.mathsAverageScore ?? "")"
        self.setupOverviewLabel(text: text)
    }
    
    func setupLoading() {
        self.heightConstrait?.constant = 80
        self.activityIndicator.startAnimating()
        self.overviewDetailsLabel.isHidden = true
    }
    
    func failedLoading() {
        self.setupOverviewLabel(text: "Unable to load SAT Scores")
    }
    
    func setupSubviews() {
        self.contentView.addSubview(self.activityIndicator)
        self.contentView.addSubview(self.overviewLabel)
        self.contentView.addSubview(self.overviewDetailsLabel)
    }
    
    func setupConstraints() {
        self.heightConstrait = self.contentView.heightAnchor.constraint(equalToConstant: 80)
        
        NSLayoutConstraint.activate([
            self.overviewLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.overviewLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.overviewLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: padding),
        ])
        
        NSLayoutConstraint.activate([
            self.overviewDetailsLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.overviewDetailsLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding),
            self.overviewDetailsLabel.topAnchor.constraint(equalTo: self.overviewLabel.bottomAnchor, constant: labelPadding),
            self.overviewDetailsLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -padding)
        ])
        
        NSLayoutConstraint.activate([
            self.activityIndicator.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
        ])
        
        self.heightConstrait?.isActive = true
    }
    
    func setupOverviewLabel(text: String) {
        let attributedText = NSMutableAttributedString(string: text)
        let nsText = text as NSString
        let textRange = NSRange(location: 0, length: nsText.length)
        attributedText.addAttribute(NSAttributedString.Key.font, value: labelFont, range: textRange)
        if text.contains("Takers") {
            let range = nsText.range(of: "Takers: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: labelBoldFont, range: range)
        }
        if text.contains("Reading Average") {
            let range = nsText.range(of: "Reading Average: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: labelBoldFont, range: range)
        }
        if text.contains("Writing Average") {
            let range = nsText.range(of: "Writing Average: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: labelBoldFont, range: range)
        }
        if text.contains("Math Average") {
            let range = nsText.range(of: "Math Average: ")
            attributedText.addAttribute(NSAttributedString.Key.font, value: labelBoldFont, range: range)
        }
        
        self.activityIndicator.stopAnimating()
        self.overviewDetailsLabel.isHidden = false
        self.overviewDetailsLabel.attributedText = attributedText
        self.heightConstrait?.constant = self.getHeight(text)
    }
    
    func getHeight(_ text: String) -> CGFloat {
        let width = UIScreen.main.bounds.width - padding * 2
        var height = text.heightWithConstrainedWidth(width, font: labelFont) + labelPadding
        height += padding * 3
        return height
    }
}
