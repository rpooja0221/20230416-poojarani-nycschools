//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import Foundation

typealias NetworkDataResult = (Data?, URLResponse?, Error?) -> Void
typealias ServiceCompletion<T> = (Result<[T], Error>) -> Void

class NetworkManager {
    private let session: URLSession
    
    required init(session: URLSession = .shared) {
        self.session = session
    }
    
    func request<T: Decodable>(_ response: T.Type, path: String, httpMethod: String = "GET", params: [String: AnyHashable]? = nil, completion: @escaping ServiceCompletion<T>)  {
        let urlString = NetworkConstants.baseURL + path
        guard let url = URL(string: urlString) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod
        
        
        let dataTask = session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            let defaultError = NSError(domain: "NYC Schools", code: 0, userInfo: nil)
            guard let self = self else {
                completion(.failure(defaultError))
                return
            }
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                self.parseSuccessData(data, completion: completion)
            } else {
                completion(.failure(defaultError))
            }
        }
        dataTask.resume()
    }
    
    
    func parseSuccessData<T: Decodable>(_ data: Data, completion: @escaping ServiceCompletion<T>) {
        do {
            let decoder = JSONDecoder()
            let schoolsList = try decoder.decode([T].self, from: data)
            completion(.success(schoolsList))
        }
        catch let error {
            completion(.failure(error))
        }
    }
}
