//
//  OptionalExtension.swift
//  NYCSchools
//
//  Created by Pooja on 4/16/23.
//

import Foundation

extension Optional where Wrapped == String {
    var stringValue: String {
        guard let wrapped = self else { return "" }
        return wrapped
    }
}
