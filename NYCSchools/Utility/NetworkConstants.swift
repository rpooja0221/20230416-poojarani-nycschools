//
//  NetworkConstants.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import Foundation

struct NetworkConstants {
    static let baseURL = "https://data.cityofnewyork.us/resource/"
    static let schoolsListPath = "s3k6-pzi2.json"
    static let schoolDetailsPath = "f9bf-2cp4.json?dbn="
}
