//
//  SceneDelegate.swift
//  NYCSchools
//
//  Created by Pooja on 4/14/23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        
        let network = NetworkManager()
        let schoolListViewModel: SchoolsListViewModelType = SchoolsListViewModel(networkManager: network)
        let schoolListViewController = SchoolsListViewController(viewModel: schoolListViewModel)
        let navigationController = UINavigationController(rootViewController: schoolListViewController)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

