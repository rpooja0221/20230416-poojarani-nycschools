//
//  SchoolsListDataModel.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import Foundation
import CoreLocation

struct NYCSchool: Decodable {
    let dbn: String
    let name: String
    let address: String?
    let city: String?
    let state: String?
    let zip: String?
    let longitude: String?
    let latitude: String?
    let description: String?
    let email: String?
    let website: String?
    var phone: String?
    
    var satScores: SchoolSatScores?
    
    var addressLine1: String {
        address.stringValue
    }
    
    var addressLine2: String {
        "\(city.stringValue), \(state.stringValue) \(zip.stringValue)"
    }
    
    var location: CLLocation? {
        guard let latitude = Double(latitude.stringValue), let longitude = Double(longitude.stringValue) else { return nil }
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case address = "primary_address_line_1"
        case city
        case state = "state_code"
        case zip
        case longitude
        case latitude
        case description = "overview_paragraph"
        case email = "school_email"
        case website
        case phone = "phone_number"
    }
}

struct SchoolSatScores: Decodable {
    let satTakers: String?
    let readingAverageScore: String?
    let mathsAverageScore: String?
    let writingAverageScore: String?
    
    enum CodingKeys: String, CodingKey {
        case satTakers = "num_of_sat_test_takers"
        case readingAverageScore = "sat_critical_reading_avg_score"
        case mathsAverageScore = "sat_math_avg_score"
        case writingAverageScore = "sat_writing_avg_score"
    }
}
