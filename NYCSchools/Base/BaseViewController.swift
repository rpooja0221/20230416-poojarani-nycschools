//
//  BaseViewController.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
    }
}
