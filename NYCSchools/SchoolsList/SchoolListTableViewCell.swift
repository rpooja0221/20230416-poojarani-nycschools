//
//  SchoolListTableViewCell.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {
    
    private let padding: CGFloat = 16
    private let labelPadding: CGFloat = 4
    private var school: NYCSchool?
    
    lazy var schoolImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .black
        imageView.image = UIImage(systemName: "building.columns.fill")
        return imageView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var addressLine1Label: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var addressLine2Label: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var mapsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        let action = UIAction(handler: { [weak self] _ in
            self?.navigateToMaps()
        })
        button.addAction(action, for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .disclosureIndicator
        self.setupSubviews()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(school: NYCSchool) {
        self.school = school
        self.nameLabel.text = school.name
        self.addressLine1Label.text = school.addressLine1
        self.addressLine2Label.text = school.addressLine2
    }
}

extension SchoolListTableViewCell {
    func setupSubviews() {
        self.contentView.addSubview(self.schoolImageView)
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addSubview(self.addressLine1Label)
        self.contentView.addSubview(self.addressLine2Label)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            self.schoolImageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: padding),
            self.schoolImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            self.schoolImageView.widthAnchor.constraint(equalToConstant: 48),
            self.schoolImageView.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        NSLayoutConstraint.activate([
            self.nameLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: padding),
            self.nameLabel.leadingAnchor.constraint(equalTo: self.schoolImageView.trailingAnchor, constant: padding/2),
            self.nameLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -padding)
        ])
        
        NSLayoutConstraint.activate([
            self.addressLine1Label.topAnchor.constraint(equalTo: self.nameLabel.bottomAnchor, constant: labelPadding),
            self.addressLine1Label.leadingAnchor.constraint(equalTo: self.nameLabel.leadingAnchor),
            self.addressLine1Label.trailingAnchor.constraint(equalTo: self.nameLabel.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.addressLine2Label.topAnchor.constraint(equalTo: self.addressLine1Label.bottomAnchor, constant: labelPadding),
            self.addressLine2Label.leadingAnchor.constraint(equalTo: self.nameLabel.leadingAnchor),
            self.addressLine2Label.trailingAnchor.constraint(equalTo: self.nameLabel.trailingAnchor),
            self.addressLine2Label.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -padding)
        ])
    }
    
    func navigateToMaps() {
        guard let latitude = school?.latitude, let longitude = school?.longitude else { return }
        guard let url = URL(string:"http://maps.apple.com/?daddr=\(latitude),\(longitude)") else { return }
        UIApplication.shared.open(url)
    }
}
