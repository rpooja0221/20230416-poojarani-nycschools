//
//  SchoolsListViewController.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import UIKit
import Combine

protocol SchoolsListViewModelType {
    var state: Published<SchoolsListState>.Publisher { get }
    var schoolsList: [NYCSchool] { get }
}

class SchoolsListViewController: BaseViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SchoolListTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        return tableView
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    lazy var errorLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private let viewModel: SchoolsListViewModelType
    private var cancellable: Set<AnyCancellable> =  []
    private let padding: CGFloat = 24.0
    private let cellIdentifier = "SchoolListTableViewCell"
    var controller: SchoolDetailsViewController?
    
    init(viewModel: SchoolsListViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.subscribeViewModel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC Schools"
        self.tableView.backgroundColor = .clear
        self.addSubviews()
        self.setupConstrains()
    }
}

private extension SchoolsListViewController {
    func subscribeViewModel() {
        self.viewModel.state
            .receive(on: RunLoop.main)
            .sink { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .loading:
                    self.tableView.isHidden = true
                    self.errorLabel.isHidden = true
                    self.activityIndicator.isHidden = false
                    self.activityIndicator.startAnimating()
                case .schoolsListLoaded:
                    self.activityIndicator.stopAnimating()
                    self.tableView.isHidden = false
                    self.errorLabel.isHidden = true
                    self.tableView.reloadData()
                case .error(let error):
                    self.activityIndicator.stopAnimating()
                    self.tableView.isHidden = true
                    self.errorLabel.isHidden = false
                    self.errorLabel.text = error
                }
            }
            .store(in: &cancellable)
    }
    
    func addSubviews() {
        self.view.addSubview(self.activityIndicator)
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.errorLabel)
    }
    
    func setupConstrains() {
        NSLayoutConstraint.activate([
            self.activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            self.errorLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            self.errorLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,
                                                     constant: padding),
            self.errorLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,
                                                      constant: -padding)
        ])
        
        NSLayoutConstraint.activate([
            self.tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

extension SchoolsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.schoolsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SchoolListTableViewCell else {
            return UITableViewCell()
        }
        if self.viewModel.schoolsList.count > indexPath.row {
            cell.setup(school: self.viewModel.schoolsList[indexPath.row])
        }
        return cell
    }
}

extension SchoolsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.viewModel.schoolsList.count > indexPath.row {
            let school = self.viewModel.schoolsList[indexPath.row]
            let schoolDetailsViewModel: SchoolDetailsViewModelType = SchoolDetailsViewModel(school: school, networkManager: NetworkManager())
            let schoolDetailsViewController = SchoolDetailsViewController(viewModel: schoolDetailsViewModel)
            self.controller = schoolDetailsViewController
            self.navigationController?.pushViewController(schoolDetailsViewController, animated: true)
        }
    }
}
