//
//  SchoolsListViewModel.swift
//  NYCSchools
//
//  Created by Pooja on 4/15/23.
//

import Foundation
import Combine

enum SchoolsListState {
    case loading
    case schoolsListLoaded
    case error(String)
}

class SchoolsListViewModel: SchoolsListViewModelType {
    
    @Published
    var _state: SchoolsListState
    var state: Published<SchoolsListState>.Publisher { self.$_state }
    
    var schoolsList: [NYCSchool] = []
    let networkManager: NetworkManager
        
    init(networkManager: NetworkManager) {
        self._state = .loading
        self.networkManager = networkManager
        self.fetchSchoolsList()
    }
    
    func fetchSchoolsList() {
        let path = NetworkConstants.schoolsListPath
        self.networkManager.request(NYCSchool.self, path: path) { [weak self] result in
            guard let self = self else { return }
            self.processSchoolsListResponse(result)
        }
    }
    
    func processSchoolsListResponse(_ result: Result<[NYCSchool], Error>) {
        switch result {
        case .success(let list):
            self._state = .schoolsListLoaded
            self.schoolsList = list
        case .failure(_):
            self._state = .error("Unable to fetch schools list")
            self.schoolsList = []
        }
    }
}
